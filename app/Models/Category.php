<?php

namespace App\Models;

use App\Models\Announcement;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory;
    protected $fillable =[
        'name_it',
        'name_en',
        'name_ua',
        'icon',
    ];
    public function announcements(){
        return $this->hasMany(Announcement::class);
    }
}
