<?php

namespace App\Models;

use App\Models\Image;
use App\Models\Category;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Announcement extends Model
{
    use HasFactory, Searchable;

    protected $fillable = [
        'title',
        'description',
        'price',
        'user_id',
        'category_id',
    ];

    public function toSearchableArray(){
        $category_it = $this->category->name_it;
        $category_en = $this->category->name_en;
        $category_ua = $this->category->name_ua;
        $array = [
            'id' => $this->id,
            'title' => $this->title,
            'description'=> $this->description,
            'category_it'=> $category_it,
            'category_en'=> $category_en,
            'category_ua'=> $category_ua,
        ];
        return $array;
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function setAccepted($value)
    {
        $this->is_accepted = $value;
        $this->save();
        return true;
    }

    public static function toBeRevisionedCount()
    {
        return Announcement::where('is_accepted' , null)->count();
    }
    public function images(){
        return $this->hasMany(Image::class);
    }
}
