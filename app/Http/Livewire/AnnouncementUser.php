<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Announcement;
use Illuminate\Support\Facades\Auth;

class AnnouncementUser extends Component
{
    public $searched;

    public function render()
    {
         if($this->searched){
            $announcements = Announcement::search($this->searched)->where('user_id', Auth::user()->id)->where('is_accepted' , true)->orderBy('created_at' , 'desc')->paginate(6);
            //  dd($announcements);

        } else {
            $announcements = Announcement::where('user_id','=',Auth::user()->id)->where('is_accepted' , true)->orderBy('created_at' , 'desc')->paginate(6);
            // dd($announcements);

        }
        return view('livewire.announcement-user', compact('announcements'));
    }
}
