<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Newsletter;

class NewsletterRegister extends Component
{
    public $email;
    protected $rules = [
        'email' => ['required','email']
    ];
    protected $messages = [
        'email.required' => "Devi inserire l'email!",
        'email.email' => "Devi inserire un email valida."
    ];

public function updated($propertyName){
  $this->validateOnly($propertyName);
}
    public function newsletter_create(){
        Newsletter::create([
            'email' => $this -> email,
        ]);
        return redirect(route('welcome'))->with('message', "Ti sei registrato alla NewsLetter!");
        $this->clean_form();
        $this->validate();
    }

    protected function clean_form(){
        $this->email="";
    }
    public function render()
    {
        return view('livewire.newsletter-register');
    }
}