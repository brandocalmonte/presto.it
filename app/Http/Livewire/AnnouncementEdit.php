<?php

namespace App\Http\Livewire;

use App\Models\Announcement;
use Livewire\Component;

class AnnouncementEdit extends Component
{
    public $announcementId;
    public $title;
    public $description;
    public $price;
    

    public function announcement_update(){
       
        $announcement = Announcement::find($this->announcementId);

        $announcement->update([
            'title' => $this->title,
            'description' => $this->description,
            'price' => $this->price,
        ]);

        return redirect(route('announcements.index'));
    }
    // public function announcement_destroy(Announcement $announcement){
    //     $announcement->delete();

    //     return redirect(route('announcements.index'))->with('message', 'Hai eliminato un annuncio');
    // }


    public function mount(){
        $announcement = Announcement::find($this->announcementId);

        $this->title = $announcement->title;
        $this->description = $announcement->description;
        $this->price = $announcement->price;
        
    }

    public function render()
    {
        return view('livewire.announcement-edit');
    }
}
