<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Category;
use App\Jobs\ResizeImage;
use App\Models\Announcement;
use Livewire\WithFileUploads;
use App\Jobs\GoogleVisionLabelImage;
use App\Jobs\GoogleVisionSafeSearch;
use App\Jobs\RemoveFaces;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;



class CreateAnnouncement extends Component
{
  use WithFileUploads;

  public $title;
  public $description;
  public $price;
  public $validated;
  public $temporary_images;
  public $images = [];
  public $image;
  public $form_id;
  public $announcements;
  public $category;


  protected $rules = [
    'title' => 'required|min:5' ,
    'price' => 'required' ,
    'description' => 'required|min:10',
    'images.*' => 'image|max:1024',
    'temporary_images.*' => 'image|max:1024',
    'category' => 'required',
  ];
  protected $messages = [
    'title.required' => "Il titolo è neccessario",
    'title.min' => "Il titolo dev'essere di almeno 5 caratteri",
   'temporary_images.required' => 'L\'immagine è richiesta.',
   'temporary_images.*.image' => 'I file devono essere immagini',
   'temporary_images.*.max' => 'L\'immagine dev\'essere massimo di 1mb.',
   'images.image' => 'L\'immagine dev\'essere un\'immagine',
   'images.max' => 'L\'immagine dev\'essere massimo di 1mb.',
  'description.required' => "La descrizione è necessaria",
    'description.min' => "La descrizione dev'essere di almeno 10 caratteri",
    'price.required' => "Devi inserire il prezzo"
  ];

  public function updatedTemporaryImages(){
    if($this->validate([
      'temporary_images.*'=>'image|max:1024',
    ])) {
      foreach ($this->temporary_images as $image){
        $this->images[] = $image;
      }
    }
  }

  public function removeImage($key){

    if (in_array($key, array_keys($this->images))){
        unset($this->images[$key]);
    }
  }

  public function store(){
    $this->validate();
    $this->announcement= Category::find($this->category)->announcements()->create($this->validate());
    $this->announcement->user()->associate(Auth::user());
    $this->announcement->save();
    if (count($this->images)){
      foreach($this->images as $image){
        // $this->announcement->images()->create(['path'=>$image->store('images', 'public')]);
        $newFileName = "announcements/{$this->announcement->id}";
        $newImage = $this->announcement->images()->create(['path'=>$image->store($newFileName,'public')]);
       
        RemoveFaces::withChain([

          new ResizeImage($newImage->path, 400, 300),
          new GoogleVisionSafeSearch($newImage->id),
          new GoogleVisionLabelImage($newImage->id)
        
        ])->dispatch($newImage->id);
      

      }


      File::deleteDirectory(storage_path('/app/livewire-tmp'));
    }

    session()->flash('message', 'Articolo inserito con successo, sarà pubblicato dopo la revisone');

    $this->clean_form();
  }


  public function updated($propertyName){
    $this->validateOnly($propertyName);
  }
  public function Announcement_store(){
    $category = Category::find($this->category);
    $announcement = $category->announcements()->create([
      'title'=>$this->title,
      'description'=>$this->description,
      'price'=>$this->price,
    ]);
    Auth::user()->announcements()->save($announcement);
    return redirect(route('announcements.index'))->with('message', 'Hai corretamente inserito il tuo annuncio');
    $this->clean_form();
    $this->validate();
  }

  protected function clean_form(){
    $this->title = "";
    $this->description = "";
    $this->price = "";
    $this->category ="";
    $this->image= "";
    $this->images= [];
    $this->temporary_images= [];
    $this->form_id = rand();
  }

  public function render()
  {
    return view('livewire.create-announcement');
  }
}
