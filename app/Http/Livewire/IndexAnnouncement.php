<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Announcement;

class IndexAnnouncement extends Component
{
    public $searched;


    // public function search_announcements(){
    //     $searched = Announcement::search($this->searched)->where('is_accepted' , true)->orderBy('created_at' , 'desc')->paginate(5);
    //     return view('livewire.index-announcement', compact('searched'));
    // }

    public function render()
    {
        if($this->searched){
            $announcements = Announcement::search($this->searched)->where('is_accepted' , true)->orderBy('created_at' , 'desc')->paginate(6);
        } else {
            $announcements = Announcement::where('is_accepted', true)->orderBy('created_at' , 'desc')->paginate(6);
        }


        return view('livewire.index-announcement', compact('announcements'));
    }
}
