<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AnnouncementController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except('announcementIndex', 'announcementShow');
    }
    public function announcementCreate(){
        $announcement = Announcement::all();

        return view('announcements.create', compact('announcement'));
    }

    public function announcementEdit(Announcement $announcement){

        return view('announcements.edit', compact('announcement'));

    }
    public function announcementShow(Announcement $announcement){
        return view('announcements.show', compact('announcement'));
    }

    public function announcement_destroy(Announcement $announcement){
        return view('announcement.index');
    }

    public function announcementIndex(){
        $announcements = Announcement::paginate(5);
        $categories = Category::all();

        return view('announcements.index', compact('announcements'), compact('categories'));
    }
    public function announcementUser(){
        $announcements = Announcement::paginate(5);
        $categories = Category::all();

        return view('announcements.user', compact('announcements'),);
    }
}
