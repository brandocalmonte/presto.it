<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Mail\BecomeRevisor;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;

class RevisorController extends Controller
{
    public function index()
    {
        // $announcement_to_checks = Announcement::all()->where('is_accepted' , null)->first();
        //   dd($announcement_to_check);
        $announcement_to_checks = Announcement::where('is_accepted', null)->first();

        return view('revisor.index' , compact('announcement_to_checks'));
    }

    public function acceptAnnouncement(Announcement $announcement)
    {
        $announcement->setAccepted(true);
        return redirect()->back()->with('message' , 'Complimenti, hai accettato l\'annuncio');
    }

    public function rejectAnnouncement(Announcement $announcement)
    {
        $announcement->setAccepted(false);
        return redirect()->back()->with('message' , 'Complimenti, hai rifiutato l\'annuncio');
    }
     public function allAnnouncement(){

         $announcements = Announcement::all();

          return view('revisor.history' ,  compact('announcements'));
     }

      public function forgetAnnouncement(Announcement $announcement)
     {
         $announcement->setAccepted(null);
         return redirect()->back()->with('message' , 'Hai riportato l\'annuncio in revisione');
     }






    public function formRevisor(){
        return view('revisor.form');
    }

    public function becomeRevisor(Request $request){
        $name = $request->input('name');
         $email = $request->input('email');
         $description = $request->input('description');

         $user_contact= compact('name', 'email', 'description');
        //  dd($user);
        // $user = implode( ',' , $user_array);

         Mail::to('admin@presto.it')->send(new BecomeRevisor( Auth::user(), $user_contact));
        return redirect()->back()->with('message', 'Complimenti! Hai richiesto di diventare revisore correttamente');

    }

    public function makeRevisor(User $user){
        // dd($user);
        Artisan::call('presto:makeUserRevisor', ["email"=>$user->email]);

        return redirect('/')->with('message', "Complimenti! l'utente è diventato revisore");
    }

}
