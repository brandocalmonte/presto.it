<?php

namespace App\Http\Controllers;

use App\Models\Newsletter;
use App\Mail\NewsletterMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;
use App\Http\Livewire\NewsletterRegister;

class AdminController extends Controller
{
    public function index(){
        return view('admin.index',);
    }
    public function newsletter(){
        return view('admin.newsletter',);
    }
    public static function send_newsletter(Request $request){
        $title = $request->input('title');
        $description = $request->input('description');
        $users = Newsletter::all();
        foreach($users as $user){
        $email= $user->email;
        $user_contact = compact( 'title', 'description', 'email');
        }
        // dd($user_contact);
        foreach($users as $user){
        //  dd($user->email);
            $contacts = Mail::to($user->email)->send(new NewsletterMail($user_contact));
            if(env('MAIL_HOST', false) == 'smtp.mailtrap.io'){
        sleep(2);
    }
        }

            return redirect()->back()->with('message', 'Hai correttamente inviato la newsletter');
}
 public function UnsubscribeNewsletter(){
        $users = Newsletter::all();
        foreach($users as $user){
            $email = $user;
        }
        Artisan::call('presto:UnsubscribeNewsletter', ["email"=>$email->email]);

        return redirect('/')->with('message', "Ti sei disiscritto dalla newsletter");
    }
}