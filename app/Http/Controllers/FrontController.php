<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Category;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FrontController extends Controller
{

    public  function welcome() {
        $announcements = Announcement::where('is_accepted', true)->orderBy('created_at', 'desc')->take(4)->get();
    return view('welcome', compact('announcements'));
    }

public function search_announcements(){
        return view('announcements.index');
    }
    public function categoryShow(Category $category){
        return view('categoryShow', compact('category'));
    }

    public function setLanguage($lang){

        session()->put ('locale', $lang);

        return redirect()->back();
    }
public function profile(){
        $user = User::where('id', Auth::user()->id)->get();
        return view('profile', compact('user'));
    }
    public function profileEdit(){

        $user = User::where('id', Auth::user()->id)->get();
        return view('profileEdit', compact('user'));

    }
    public function faq(){
        return view('faq');
    } public function terms(){
        return view('terms');
    } public function privacy(){
        return view('privacy');
    }

}
