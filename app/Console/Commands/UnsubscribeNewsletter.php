<?php

namespace App\Console\Commands;

use App\Models\Newsletter;
use Illuminate\Console\Command;

class UnsubscribeNewsletter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'presto:UnsubscribeNewsletter {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Disiscriviti dalla newsletter';

    /**
     * Execute the console command.
     *
     * @return int
     */
     public function __contruct()
    {
        parent::__contruct();
    }
    public function handle()
    {
        $email = Newsletter::where('email', $this->argument('email'))->first();
        if(!$email){
            $this->error ('Utente non trovato');
            return;
        }
        $email->delete();
        $this->info("L'email {$email->email} è stata eliminata dalla newsletter.");
    }
}
