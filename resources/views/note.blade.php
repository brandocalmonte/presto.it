{{-- SWIPER --}}
<section class="my-4">
    <h1 class="text-center">Gli ultimi articoli:</h1>
    <div class="container-fluid my-5">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-6 text-center">
                <div class="swiper mySwiper">
                    <div class="swiper-wrapper">
                      <div class="swiper-slide"><img src="https://picsum.photos/600" alt=""></div>
                      <div class="swiper-slide"><img src="https://picsum.photos/600" alt=""></div>
                      <div class="swiper-slide"><img src="https://picsum.photos/600" alt=""></div>
                      <div class="swiper-slide"><img src="https://picsum.photos/600" alt=""></div>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-pagination"></div>
                  </div>
            </div>
        </div>
    </div>
</section>