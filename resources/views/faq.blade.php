<x-layout>
    <x-slot name="title">Presto.it_FaQ</x-slot>

<div class="container-fluid pt-4">
        <div class="row">
            <div class="col-12 text-o pb-5 text-center">
                <h1 class="display-5 border-bottom">
                    Faq
                </h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-around align-items-center">
            <div class="col-12 col-lg-6 text-center">
                <img src="/media/faq.png" alt="" class="img-fluid">
            </div>
            <div class="col-12 col-lg-5">


            <button class="accordion accordion-custom ">{{__('ui.howToRegister')}}</button>
            <div class="panel  border-accordion-custom ">
            <p class="py-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            </div>

            <button class="accordion accordion-custom">{{__('ui.howToPost')}}</button>
            <div class="panel  border-accordion-custom">
            <p class="py-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            </div>

            <button class="accordion accordion-custom">{{__('ui.howToEditPass')}}</button>
            <div class="panel  border-accordion-custom">
            <p class="py-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</pc>
            </div>

            <button class="accordion accordion-custom bb-custom    ">{{__('ui.wantToWorkWithUs')}}</button>
            <div class="panel  border-accordion-custom">
            <p class="py-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            </div>




        </div>
    </div>

</div>
</x-layout>