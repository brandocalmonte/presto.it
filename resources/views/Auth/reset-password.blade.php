<x-layout>
    <x-slot name="title">{{__('ui.resetPassword')}}</x-slot>

    </div> <div class="container-fluid pt-4">
        <div class="row">
            <div class="col-12 text-o pb-5 text-center">
                <h1 class="display-5 border-bottom">
                    {{__('ui.resetPassword')}}
                </h1>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (session('status'))
        <div class="mb-4 alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="container my-5">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-8">

                <form class="p-5 border shadow" method="POST" action="/reset-password">

                    @csrf

                    <input type="hidden" name="token" value="{{request()->route('token')}}">

                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">{{__('ui.email')}}</label>
                      <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$request->email}}">
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">{{__('ui.password')}}</label>
                        <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputPasswordConfirmation" class="form-label">{{__('ui.passwordConf')}}</label>
                        <input type="password" name="password_confirmation" class="form-control" id="exampleInputPasswordConfirmation">
                    </div>

                    <button type="submit" class="btn custom1-btn btn-c">{{__('ui.resetPassword')}}</button>
                </form>

            </div>
        </div>
    </div>

</x-layout>