<x-layout>
    <x-slot name="title">{{__('ui.register')}}</x-slot>

    <div class="container">
        <div class="row justify-content-center py-5">
                <div class="col-12 col-lg-6 shadow rounded-borders p-5">
                    <p class="display-5 fw-bold lead text-o text-center">{{__('ui.register')}}</p>

                        <form action="{{route('register')}}" method="POST">

                            @csrf
                            <div class="mb-3">
                                <label for="exampleFormControlName" class="form-label text-o">{{__('ui.name')}}</label>
                                <input type="text" name="name" class="form-control" id="formControlName"  placeholder="Mario">
                            </div>
                            <div class="mb-3">
                                <label for="exampleFormControlSurname" class="form-label text-o">{{__('ui.surname')}}</label>
                                <input type="text" name="surname" class="form-control" id="formControlSurname"  placeholder="Rossi">
                            </div>
                            <div class="mb-3">
                                <label for="formControlGender" class="form-label text-o d-block">{{__('ui.gender')}}</label>
                                <select name="gender" id="formControlGender">
                                    <option value="M">Uomo</option>
                                    <option value="F">Donna</option>
                                    <option value="Other">Altro</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="exampleFormControlEmail" class="form-label text-o">{{__('ui.email')}}</label>
                                <input type="email" name="email" class="form-control" id="formControlEmail" placeholder="name@example.com">
                            </div>
                            <div class="mb-3">
                                <label for="exampleFormControlPassword" class="form-label text-o">{{__('ui.password')}}</label>
                                <input type="password" name="password" class="form-control" id="formControlPassword">
                            </div>
                            <div class="mb-3">
                                <label for="exampleFormControlPassword" class="form-label text-o">{{__('ui.passwordConf')}}</label>
                                <input type="password" name="password_confirmation" class="form-control" id="formControlPasswordConfirmation">
                            </div>
                            <div class="mb-3 d-flex justify-content-center">
                                <button type="submit" class="btn btn-custom">{{__('ui.register')}}</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>


        </div>
    </div>










</x-layout>