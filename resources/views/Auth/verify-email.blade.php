<x-layout>
    <x-slot name="title">{{__('ui.thanks4Subscribe')}}</x-slot>


    </div> <div class="container-fluid pt-4">
        <div class="row">
            <div class="col-12 text-o pb-5 text-center">
                <h1 class="display-5 border-bottom">
                    {{__('ui.verifyEmail')}}
                </h1>
            </div>
        </div>
    </div>

    <div class="container my-5">
        <div class="row justify-content-center custom-box">
            <div class="col-12 col-md-6">
                <h2>{{__('ui.guideVerifyEmail')}}</h2>
            </div>

            @if (session('status') == 'verification-link-sent')
                <div class="mb-4 alert alert-success">
                    {{__('ui.linkSent')}}
                </div>
            @endif
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 my-5 d-flex align-items-center flex-column">
                <form action="{{route('verification.send')}}" method="post">
                    @csrf

                    <button type="submit" class="my-2 btn custom1-btn    btn-c">{{__('ui.sendMailAgain')}}</button>
                </form>

                <form action="{{route('logout')}}" method="post">
                    @csrf

                    <button type="submit" class="btn custom1-btn    btn-c">
                        {{__('ui.logout')}}
                    </button>
                </form>
            </div>
        </div>
    </div>

</x-layout>