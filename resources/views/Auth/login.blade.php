<x-layout>
    <x-slot name="title">{{__('ui.login')}}</x-slot>

    <div class="container">
        <div class="row justify-content-center py-5">
            <div class="col-12 col-lg-6 shadow rounded-borders p-5">
                <p class="display-5 fw-bold lead text-o text-center">{{__('ui.login')}}</p>
                    <form action="{{route('login')}}" method="POST">

                        @csrf

                        <div class="mb-3">
                            <label for="exampleFormControlEmail" class="form-label text-o">{{__('ui.email')}}</label>
                            <input type="email" name="email" class="form-control" id="exampleFormEmail" placeholder="name@example.com">
                        </div>
                        <div class="mb-3">
                            <label for="exampleFormControlPassword " class="form-label text-o">{{__('ui.password')}}</label>
                            <input type="password" name="password" class="form-control" id="exampleFormPassword">
                        </div>
                        <div class="mb-3 d-flex justify-content-center">
                            <button type="submit" class="btn btn-custom">{{__('ui.signIn')}}</button>
                        </div>
                        <div class="col-12  text-center">
                            <div>
                            <a href="/forgot-password"class="text-o">{{__('ui.forgotPassword')}}</a>
                        </div>
                        <div>
                            <a href="{{route('register')}}" class="text-o">{{__('ui.notRegistered')}}</a>

                        </div>

                        </div>

                    </form>
            </div>
        </div>
    </div>







</x-layout>