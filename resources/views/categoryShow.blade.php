<x-layout>
    @if(session('locale')=='it')
                    <x-slot name="title">{{$category->name_it}}</x-slot>

                @elseif(session('locale')=='en')
                    <x-slot name="title">{{$category->name_en}}</x-slot>

                @else
                   <x-slot name="title">{{$category->name_ua}}</x-slot>

                @endif


    <div class="container-fluid text-o my-5">
        <div class="row">
            <div class="col-12 text-center">
    @if(session('locale')=='it')

                <h2 class="display-5 border-bottom">{{__('ui.exploreCategory')}} {{$category->name_it}}</h2>
                   @elseif(session('locale')=='en')

                <h2 class="display-5 border-bottom">{{__('ui.exploreCategory')}} {{$category->name_en}}</h2>
                   @else

                <h2 class="display-5 border-bottom">{{__('ui.exploreCategory')}} {{$category->name_ua}}</h2>
                @endif
            </div>
        </div>
    </div>


    <div class="container my-5">
        <div class="row justify-content-around px-3">

            @forelse ($category->announcements as $announcement)
            <div class="col-12 col-md-3 article-box1 mb-5 py-2 card my-5">
                        <div class="text-center imgBox">
                            <img src="{{ !$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(400, 300) : '/media/placeholder.png' }}"
                            alt="{{ $announcement->title }}" class="img-fluid my-2 img-article">

                        </div>
                        <div class="contentBox">
                            <h4 class="card-title text-center text-bl">{{ $announcement->title }}</h5>
                                 @if(session('locale')=='it')
                            <p class="p-article lead text-cards fw-bold text-center text-bl">{{__('ui.categoryAnn')}} : {{$announcement->category->name_it}}</p>
                            {{-- @dd($category) --}}
                            @elseif(session('locale')=='en')
                            <p class="p-article lead text-cards fw-bold text-center text-bl">{{__('ui.categoryAnn')}} : {{$announcement->category->name_en}}</p>
                            @else
                            <p class="p-article lead text-cards fw-bold text-center text-bl">{{__('ui.categoryAnn')}} : {{$announcement->category->name_ua}}</p>
                            @endif
                            <p class="p-article lead text-bl text-cards fw-bold text-end">€ {{ $announcement->price }}</p>
                            <p class="p-article lead text-bl text-cards fst-italic pt-2 text-end">{{__('ui.author')}} : {{ $announcement->user->name }}</p>
                             <a href="{{ route('announcements.show', compact('announcement')) }}" class="buy">{{__('ui.details')}}</a>


                        </div>

             </div>
                        @empty
                        <div class="col-12">
                            <div class="alert alert-warning py-3 shadow">
                                <p class="lead">{{__('ui.noResults')}}</p>
                            </div>
                        </div>
                        @endforelse
             </div>
        </div>
    </div>




</x-layout>