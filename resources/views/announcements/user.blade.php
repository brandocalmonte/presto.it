<x-layout>
    <x-slot name="title">{{__('ui.userAnn')}}</x-slot>


    <div class="container-fluid">
        <div class="row justify-content-center pt-3" data-aos="flip-left" data-aos-easing="ease-out-cubic"
            data-aos-duration="2000">
            <div class="col-12">
                <h1 class="text-o display-5 text-center border-bottom">{{__('ui.userAnn')}}</h1>
            </div>
        </div>
    </div>



                    <livewire:announcement-user />
                    <div class="container">
                        <div class="row">
                            <div class="col-12 d-flex justify-content-center">
                                {{$announcements->links()}}

                            </div>
                        </div>
                    </div>







</x-layout>