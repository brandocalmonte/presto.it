<x-layout>
    <x-slot name="title">{{$announcement->title}}</x-slot>


    <div class="container-fluid text-o">
        <div class="row justify-content-center py-5">
            <div class="col-12  text-center" data-aos="flip-left"
            data-aos-easing="ease-out-cubic"
            data-aos-duration="2000">
                <h1 class="text-o display-6">{{__('ui.titleAnn')}}{{$announcement->title}}</h1>
            </div>
        </div>
    </div>


    <div class="container my-5">
        <div class="row justify-content-center box-img-show">
            <div class="col-12">


                    <h1 class="text-o text-center fw-bold py-5 display-5">{{$announcement->title}}</h1>

            </div>
            <div class="col-12 col-md-6 d-flex justify-content-center flex-column align-items-center pb-5 ">

                        @if(count($announcement->images) > 0)

                    <div class="swiper mySwiper swiper-custom shadow" data-aos="fade-right"
                data-aos-offset="300"
                data-aos-easing="ease-in-sine">
                <div class="swiper-wrapper">

                    @foreach ($announcement->images as $image)
                     <div class="p-0 swiper-slide @if($loop->first) active @endif"><img src="{{$image->getUrl(400,300)}}" alt="" class="img-fluid"></div>

                    @endforeach
                </div>

                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>

                  </div>
                @else
                <div class="bg-placeholder d-flex justify-content-center align-items-center">
                    <p class="text-o display-4 fw-bold">Presto.it</p>
                </div>
                @endif
     </div>



                <div class="col-12 col-md-4 d-flex align-items-start flex-column" data-aos="fade-left"
                data-aos-offset="300"
                data-aos-easing="ease-in-sine">
                    @if(session('locale')=='it')
                            <p class="lead text-bl fw-bold">{{__('ui.categoryAnn')}} : {{$announcement->category->name_it}}</p>
                            @elseif(session('locale')=='en')
                            <p class="lead text-bl fw-bold">{{__('ui.categoryAnn')}} : {{$announcement->category->name_en}}</p>
                            @else
                            <p class="lead text-bl fw-bold">{{__('ui.categoryAnn')}} : {{$announcement->category->name_ua}}</p>
                            @endif

                    <p class="lead text-bl fst-italic ">{{$announcement->description}}</p>

                                  <p class="lead text-bl ">€ {{$announcement->price}}</p>
                                  @auth
                    @if(Auth::user()->id == $announcement->user->id)
                              <a type="submit" href="{{route('announcements.edit', compact('announcement'))}}" class=" btn custom1-btn btn-c text-o shadow">{{__('ui.btnEdit')}}</a>
                    @endif
                    @endauth

                </div>

                <div class="col-12 d-flex justify-content-end">
                    <p class="lead text-bl fst-italic ">{{__('ui.author')}} : {{ $announcement->user->name }}</p>



                </div>
            </div>
        </div>






</x-layout>