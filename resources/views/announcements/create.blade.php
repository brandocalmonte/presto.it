<x-layout>
    <x-slot name="title">{{__('ui.createAnn')}}</x-slot>


    <div class="container">
        <div class="row justify-content-center py-5" data-aos="flip-left"
        data-aos-easing="ease-out-cubic"
        data-aos-duration="2000">
            <div class="col-12 col-md-8 shadow p-5 rounded-borders">
                <livewire:create-announcement />
            </div>
        </div>
    </div>


</x-layout>
