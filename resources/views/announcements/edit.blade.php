<x-layout>
    <x-slot name="title">{{__('ui.editAnn')}}</x-slot>

    <div class="container">
        <div class="row justify-content-center py-5">
            <div class="col-12 col-md-8">
                <livewire:announcement-edit announcementId="{{$announcement->id}}">
            </div>
        </div>
    </div>


</x-layout>
