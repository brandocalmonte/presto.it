<x-layout>
    <x-slot name="title">{{Auth::user()->name}}</x-slot>

    <div class="container-fluid pt-4">
        <div class="row">
            <div class="col-12 text-o pb-5 text-center">
                <h1 class="display-5 border-bottom">
                   {{Auth::user()->name}}
                </h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-around align-items-center">
            <div class="col-12 col-lg-6 text-center">
                <img src="/media/edit.png" alt="" class="img-fluid">
            </div>
            <div class="col-12 col-lg-5">
                <p class="display-3 border-bottom text-center">{{__('ui.editInfo')}}</p>
                <p class="fst-italic text-center fs-4 fw-light">{{__('ui.whatToEdit')}}</p>
                <div class="text-center"><a href="{{route('profileEdit')}}" class="btn custom1-btn custom2-btn btn-c">{{__('ui.clickHere')}}</a></div>
        </div>
    </div>
    <div class="row justify-content-around align-items-center">
        <div class="col-12 col-lg-5">
                <p class="display-3 border-bottom text-center">{{__('ui.lookYourAnn')}}</p>
                <p class="fst-italic text-center fs-4 fw-light">{{__('ui.checkYourAnn')}}</p>
                <div class="text-center"><a href="{{route('announcements.user')}}" class="btn custom1-btn custom2-btn btn-c">{{__('ui.clickHere')}}</a></div>
        </div>
            <div class="col-12 col-lg-6 text-center">
                <img src="/media/annunci.png" alt="" class="img-fluid">
            </div>

    </div>
    </div>
</x-layout>