 <nav class="navbar navbar-expand-lg navbar-light bg-w shadow sticky-top">
  <div class="container-fluid mx-auto main-dropdown px-3">
      <a class="navbar-brand" href="{{route('welcome')}}"><img src="/media/logo.png" alt="" class="img-fluid logo-custom"></a>
    <button class="navbar-toggler bg-o" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse main-dropdown" id="navbarSupportedContent">
    <ul class="navbar-nav mx-auto mb-2 mb-lg-0 ">
        <li class="nav-item">
          <a class="nav-link mobile-margin" aria-current="page" href="{{route('welcome')}}"><span class="nav-custom">{{__('ui.navHome')}}</span></a>
        </li>
        <li class="nav-item">
          @auth<a class="nav-link" aria-current="page" href="{{route('announcements.create')}}"><span class="nav-custom">{{__('ui.navAddAnnounce')}}</span></a>@endauth
        </li>

        @auth
        @if (Auth::user()->is_revisor)
        <li class="nav-item">
          <a class="nav-link btn border-revisor btn-sm position-relative" aria-current="page" href="{{route('revisor.index')}}"><span class="nav-custom">{{__('ui.navRevisor')}}</span>
            <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-o">{{App\Models\Announcement::toBeRevisionedCount()}}<span class="visually-hidden">unread messages</span>
          </span>
          </a>
        </li>
        @endif
        @endauth

        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="{{route('announcements.index')}}"><span class="nav-custom">{{__('ui.navAllAnnouncements')}}</span></a>
        </li>
    </ul>





    @guest
    <ul class="navbar-nav  mb-2 mb-lg-0">
        <li class="nav-item dropdown login-dropdown">
          <a class="nav-link dropdown-toggle " href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"><span class="nav-custom ">{{__('ui.navLogReg')}}</span></a>


          <ul class="dropdown-menu login-dropdown" aria-labelledby="navbarDropdown">
            <li><form class="dropdown-item no-hover2" action="{{route('login')}}" method="POST">

                        @csrf

                        <div class="mb-3">
                            <label for="exampleFormControlEmail" class="form-label text-o">{{__('ui.email')}}</label>
                            <input type="email" name="email" class="form-control" id="exampleFormEmail" placeholder="name@example.com">
                        </div>
                        <div class="mb-3">
                            <label for="exampleFormControlPassword " class="form-label text-o">{{__('ui.password')}}</label>
                            <input type="password" name="password" class="form-control" id="exampleFormPassword">
                        </div>
                        <div class="mb-3 d-flex justify-content-center">
                            <button type="submit" class="btn btn-custom">{{__('ui.signIn')}}</button>
                        </div>
                        <div class="col-6  text-center">
                            <a href="/forgot-password" class="text-o ">{{__('ui.forgotPassword')}}</a>
                        </div>

                        <div class="col-6  text-center">
                            <a href="{{route('register')}}" class="text-o">{{__('ui.notRegistered')}}</a>

                        </div>

                    </form></li>
            @else
            <ul class="navbar-nav  mb-2 mb-lg-0 ">
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  <span class="nav-custom">{{__('ui.navWelcomeName')}} {{Auth::user()->name}}</span>
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                  @auth
        @if (Auth::user()->is_admin)
        <li class="nav-item ">
          <a class="dropdown-item" aria-current="page" href="{{route('admin.index')}}"><span class="nav-custom">{{__('ui.navAdmin')}}</span>
          </a>
        </li>
        @endif
        @endauth
        <li class="nav-item ">
          <a class="dropdown-item" aria-current="page" href="{{route('profile')}}"><span class="nav-custom">{{__('ui.profile')}}</span>
          </a>
        </li>

            <li><a class="dropdown-item" href="#" onclick="event.preventDefault(); document.querySelector('#form-logout').submit();"><span class="nav-custom">{{__('ui.logout')}}</span></a></li>
            <form action="{{route('logout')}}" method="POST" id="form-logout" class="d-none">@csrf</form>
            @endguest
          </ul>
        </li>
      </ul>
    </ul>
 <ul class="navbar-nav  mb-2 mb-lg-0">
        <li class="nav-item dropdown lang-dropdown">
          <a class="nav-link dropdown-toggle " href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-expanded="false"><span class="nav-custom ">
            {{__('ui.navLang')}}</span></a>
          <ul class="dropdown-menu lang-bg" aria-labelledby="navbarDropdown1">
            <div class="lang-flex">
              <li class="dropdown-item no-hover3">
                <x-_locale lang="it" nation="it"/>
              </li>
              <li class="dropdown-item no-hover3">
              <x-_locale lang="en" nation="gb"/>
              </li>
              <li class="dropdown-item no-hover3">
              <x-_locale lang="ua" nation="ua"/>
              </li>

            </div>
      </ul>
    </ul>
    </div>

  </div>
</nav>