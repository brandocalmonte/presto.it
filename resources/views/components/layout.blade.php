<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    {{-- CSS  --}}
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <!-- animated on scroll -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <title> {{$title?? 'Presto.it'}}</title>
    <link rel="icon" href="/media/logo.png" target="_blank">


    @livewireStyles
</head>
<body>

    <x-navbar />

        {{$slot}}



    <x-footer />


    {{--FontAwesome--}}
    <script src="https://kit.fontawesome.com/3f89005f9b.js" crossorigin="anonymous"></script>
    @livewireScripts

    <script src="https://unpkg.com/swiper@8/swiper-bundle.min.js"></script>
<script>

    const swiper = new Swiper('.swiper', {
  // Optional parameters
  loop: true,

  spaceBetween: 30,
        effect: "fade",

  // If we need pagination
  pagination: {
    el: '.swiper-pagination',
  },

  // Navigation arrows
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },

  // And if we need scrollbar
  scrollbar: {
    el: '.swiper-scrollbar',
  },
});
</script>

    {{-- JS --}}
    <script src="{{asset('js/app.js')}}"></script>
    <!-- animated on scroll -->
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

        <script>
        AOS.init();
        </script>
</body>
</html>