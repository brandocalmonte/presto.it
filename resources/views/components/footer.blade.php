<!-- Footer Start -->
<div class="container-fluid bg-o text-white mt-5">
    <div class="row pt-5">
        <div class="col-lg-7 col-md-6">
            <div class="row">
                <div class="col-md-6 mb-5">
                    <h3 class="text-bl mb-4">{{__('ui.contactsFooter')}}</h3>
                    <p><i class="fa fa-map-marker-alt mr-2"></i> Via 123, Bari, IT</p>
                    <p><i class="fa fa-phone-alt mr-2"></i> +012 345</p>
                    <p><i class="fa fa-envelope mr-2"></i> info@presto.it</p>
                    <div class="d-flex justify-content-start mt-4">
                        <a class="btn btn-outline-light btn-social" href="#"><i class="fab fa-twitter icon-width"></i></a>
                        <a class="btn btn-outline-light btn-social" href="#"><i class="fab fa-facebook-f icon-width"></i></a>
                        <a class="btn btn-outline-light btn-social" href="#"><i class="fab fa-linkedin-in icon-width"></i></a>
                        <a class="btn btn-outline-light btn-social" href="#"><i class="fab fa-instagram icon-width"></i></a>
                    </div>
                </div>
                <div class="col-md-6 mb-5">
                    <h3 class="text-bl  mb-4"> {{__('ui.linkFooter')}}</h3>
                    <div class="d-flex flex-column justify-content-start ">
                        <a class="text-white mb-2" href="{{route('welcome')}}"><i class="fa fa-angle-right mr-2"></i>{{__('ui.linkHome')}}</a>
                        <a class="text-white mb-2" href="{{route('announcements.create')}}"><i class="fa fa-angle-right mr-2"></i>{{__('ui.linkAddAnouc')}}</a>
                        <a class="text-white mb-2" href="{{route('announcements.index')}}"><i class="fa fa-angle-right mr-2"></i>{{__('ui.linkShowAnounc')}}</a>
                        {{-- <a class="text-white mb-2" href="#"><i class="fa fa-angle-right mr-2"></i>{{__('ui.linkAboutUs')}}</a> --}}
                        <a class="text-white mb-2" href="{{route('revisor.form')}}"><i class="fa fa-angle-right mr-2"></i>{{__('ui.linkWorkUs')}}</a></div>
                </div>
            </div>
        </div>
        <div class="col-lg-5 col-md-6 mb-5">
            <h3 class="text-bl mb-4">{{__('ui.newsletterFooter')}}</h3>
            <p>Rebum labore lorem dolores kasd est, et ipsum amet et at kasd, ipsum sea tempor magna tempor. Accu kasd sed ea duo ipsum. Dolor duo eirmod sea justo no lorem est diam</p>
            <div class="w-100">
                <livewire:newsletter-register  />
            </div>
        </div>
    </div>
</div>
<div class="container-fluid bg-bl text-white py-4 px-0 mx-0">
        <div class="row p-0 m-0">
            <div class="col-lg-6 text-center text-md-left mb-3 mb-md-0 px-0">
                <p class="m-0 text-white">&copy; <a href="#" class="text-o">PRESTO.it</a>. All Rights Reserved.
        Designed by TeamNameNotFound

                </p>
            </div>
            <div class="col-lg-6 text-center text-md-right px-0 mb-3 mb-md-0">
                <ul class="nav d-inline-flex">
                    <li class="nav-item">
                        <a class="nav-link text-white py-0" href="{{route('privacy')}}">Privacy</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white py-0" href="{{route('terms')}}">Terms</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white py-0" href="{{route('faq')}}">FAQs</a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</div>


<!-- Footer End -->