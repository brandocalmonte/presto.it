<div>
        <form class="input-group" wire:submit.prevent="newsletter_create">
                    <input wire:model.lazy="email" type="email" class="form-control border-light mx-2
                    @if (isset($email)) : @error('email') is-invalid @else is-valid @enderror @endif"  placeholder="Scrivi qui la tua email">
                    <div class="input-group-append">
                        <button type="submit" class="btn custom1-btn btn-d px-4">{{__('ui.btnFooter')}}</button>
                    </div>
                    </form>

                    @error('email')
                <span class="fst-italic text-w small">{{ $message }}</span>
            @enderror
</div>
