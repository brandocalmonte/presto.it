<div>
    <div>

        <h1 class="text-o text-center">{{__('ui.editAnn')}} !</h1>

        <form class="py-5" wire:submit.prevent="announcement_update" enctype="multipart/form-data">



            @if (session()->has('message'))
                <div class="alert alert-sucess">
                    {{ session('message') }}
                </div>
            @endif


            <div class="mb-3">
                <label for="title">{{__('ui.annName')}}</label>
                <input type="text" wire:model.lazy="title" id="title"
                    class="form-control @if (isset($title)) : @error('title') is-invalid @else is-valid @enderror @endif">
                @error('title')
                    <span class="fst-italic text-danger small">{{ $message }}</span>
                @enderror
            </div>
            <div class="mb-3">
                <label for="description">{{__('ui.commentsAnn')}}</label>
                    <textarea  wire:model.lazy="description" class="form-control @if (isset($description)) : @error('description') is-invalid @else is-valid @enderror @endif" placeholder="Scrivi una piccola description" id="description"></textarea>
                @error('description')
                    <span class="fst-italic text-danger small">{{ $message }}</span>
                @enderror
            </div>
            <div class="mb-3">
                <label for="price">{{__('ui.priceAnn')}}</label>
                <input type="number" wire:model.lazy="price" id="price"
                class="form-control @if (isset($price)) : @error('price') is-invalid @else is-valid @enderror @endif" placeholder="99.99">
               @error('price')
                <span class="fst-italic text-danger small">{{ $message }}</span>
            @enderror
            </div>
            <button type="submit" class="btn custom1-btn btn-c text-o shadow">{{__('ui.btnEdit')}}</button>
            {{-- @Auth<button wire:click="announcement_destroy({{$announcement}})" class="btn btn-custom text-o p-3 shadow">Elimina</button>@endAuth --}}

        </form>





    </div>

</div>
