<div>

    @if (session()->has('message'))
        <div class="alert alert-sucess bg-o">
            <p class="lead text-bl">{{ session('message') }}</p>
        </div>
    @endif
    <!--FORM-->
    <div class="container">
        <div class="row justify-content-center">

    <form class="col-12 col-md-9 my-5 py-3 shadow-filter shadow bg-w">
        <div class="row justify-content-evenly align-items-center">
            <div class="mb-3 col-12 col-md-10 text-center d-flex flex-column">
                <label for="searched" class="h5 text-o">{{__('ui.searchByName')}}</label>

                <input wire:model="searched" class="form-control me-2 form-custom-o" type="search" placeholder="Search"
                    aria-label="Search" id="searched">


            </div>
        </div>
    </form>
        </div>
    </div>

    <!--FINE FORM-->


{{-- @dd($announcements, $searched) --}}
    <div class="container">
        <div class="row justify-content-around px-3">
            @forelse ($announcements as $announcement)
                <div class="col-12 col-md-3 article-box1 mb-5 py-0 card my-3 mx-1">
                        <div class="text-center imgBox">
                            <img src="{{ !$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(400, 300) : '/media/placeholder.png' }}"
                            alt="mouse corsair" class="img-fluid my-2 img-article">

                        </div>
                        <div class="contentBox">
                            <h4 class="card-title text-center text-bl">{{ $announcement->title }}</h5>
                             @if(session('locale')=='it')
                            <p class="p-article lead text-cards fw-bold text-center text-bl">{{__('ui.categoryAnn')}} : {{$announcement->category->name_it}}</p>
                            @elseif(session('locale')=='en')
                            <p class="p-article lead text-cards fw-bold text-center text-bl">{{__('ui.categoryAnn')}} : {{$announcement->category->name_en}}</p>
                            @else
                            <p class="p-article lead text-cards fw-bold text-center text-bl">{{__('ui.categoryAnn')}} : {{$announcement->category->name_ua}}</p>
                            @endif
                            <p class="p-article lead text-bl text-cards fw-bold text-end">€ {{ $announcement->price }}</p>
                            <p class="p-article lead text-bl text-cards fst-italic pt-2 text-end">{{__('ui.author')}} : {{ $announcement->user->name }}</p>
                             <a href="{{ route('announcements.show', compact('announcement')) }}" class="buy">{{__('ui.details')}}</a>


                        </div>

             </div>
                @empty
                    <div class="col-12">
                        <div class="alert alert-warning py-3 shadow">
                            <p class="lead">{{ __('ui.noResults') }}</p>
                        </div>
                    </div>
        </div>

            @endforelse
    </div>

</div>
</div>
