<div>

    <h1 class="text-o text-center">{{__('ui.createAnn')}}</h1>

    <form class="py-5" wire:submit.prevent="store" enctype="multipart/form-data">

        {{-- @csrf --}}

        @if (session()->has('message'))
            <div class="alert alert-sucess">
                {{ session('message') }}
            </div>
        @endif


        <div class="mb-3">
            <label for="title">{{__('ui.annName')}}</label>
            <input type="text" wire:model.lazy="title" id="title"
                class="form-control @if (isset($title)) : @error('title') is-invalid @else is-valid @enderror @endif">
            @error('title')
                <span class="fst-italic text-danger small">{{ $message }}</span>
            @enderror
        </div>
        <div class="mb-3">
            <label for="description">{{__('ui.commentsAnn')}}</label>
                <textarea  wire:model.lazy="description" class="form-control @if (isset($description)) : @error('description') is-invalid @else is-valid @enderror @endif" placeholder="Scrivi una piccola description" id="description"></textarea>
            @error('description')
                <span class="fst-italic text-danger small">{{ $message }}</span>
            @enderror
        </div>
        <div class="mb-3">
            <label for="category">{{__('ui.categoryAnn')}}</label>
            <select wire:model.defer="category" id="category" class="form-control">
                <option value="">{{__('ui.selectCategoryAnn')}}</option>
                @foreach ($categories as $category)
                             @if(session('locale')=='it')
                                <option value="{{$category->id}}">{{$category->name_it}}</option>
                                @elseif(session('locale')=='en')
                                <option value="{{$category->id}}">{{$category->name_en}}</option>
                                @else
                                <option value="{{$category->id}}">{{$category->name_ua}}</option>
                                @endif
                @endforeach
            </select>
        </div>
        <div class="mb-3">
    <input wire:model="temporary_images" type="file" name="images" multiple class="form-control shadow @error('temporary_images.*')is-invalid @enderror"
    placeholder="Img"/>
    @error('temporary_images.*')
         <p class="text-danger mt-2">{{$message}}</p>
    @enderror
</div>
@if(!empty($images))
<div class="row">
    <div class="col-12">
        <p>{{__('ui.photoPreview')}}</p>
        <div class="row border border-4 border-info rounded shadow py-4">
            @foreach ($images as $key => $image)
            <div class="col my-3">
                <div class="img-preview mx-auto shadow rounded" style="background-image: url({{$image->temporaryUrl()}});"></div>
                <button type="button" class="btn custom1-btn btn-c d-block text-center mt-2 mx-auto" wire:click="removeImage({{$key}})">{{__('ui.deletePhoto')}}</button>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endif
        <div class="mb-3">
            <label for="price">{{__('ui.priceAnn')}}</label>
            <input type="number" wire:model.lazy="price" id="price"
            class="form-control @if (isset($price)) : @error('price') is-invalid @else is-valid @enderror @endif" placeholder="99.99">
           @error('price')
            <span class="fst-italic text-danger small">{{ $message }}</span>
        @enderror
        </div>
        <button type="submit" class="btn custom1-btn btn-c text-o shadow">{{__('ui.btnCreateAnn')}}</button>


    </form>





</div>
