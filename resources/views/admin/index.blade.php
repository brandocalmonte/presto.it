<x-layout>
    <x-slot name="title">{{__('ui.titleAdmin')}}</x-slot>

    <div class="container-fluid pt-4">
        <div class="row">
            <div class="col-12 text-o pb-5 text-center">
                <h1 class="display-5 border-bottom">
                    {{__('ui.titleAdmin')}}
                </h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-around align-items-center">
            <div class="col-12 col-lg-6 text-center">
                <img src="/media/Notifications.png" alt="" class="img-fluid">
            </div>
            <div class="col-12 col-lg-5">
                <p class="display-3 border-bottom text-center">Newsletter</p>
                <p class="fst-italic text-center fs-4 fw-light">{{__('ui.textAdmin')}}</p>
                <div class="text-center"><a href="{{route('admin.newsletter')}}" class="btn custom1-btn custom2-btn btn-c">{{__('ui.sendNewsletter')}}</a></div>
        </div>
    </div>
    </div>
</x-layout>