<x-layout>
    <x-slot name="title">{{__('ui.sendNewsletter')}}</x-slot>
    <div class="container-fluid pt-4">
        <div class="row">
            <div class="col-12 pb-4 text-center">
                <h1 class="display-5 border-bottom text-o">
                    Newsletter
                </h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-6 shadow rounded-borders p-5">
                <form method="POST" action="{{route('admin.send-newsletter')}}" >
                    @csrf
                    <div class="mb-3">
                        <label for="title">{{__('ui.newsletterTitle')}}</label>
                        <input type="text" name="title" id="title"
                        class="form-control">
                    </div>
                    <div class="mb-3">
                        <label for="description">{{__('ui.newsletterDescr')}}</label>
                        <textarea  name="description" class="form-control "  id="description"></textarea>
                    </div>

                    <button type="submit" class="btn custom1-btn btn-c text-o  shadow">{{__('ui.newsletterCreate')}}</button>


                </form>
            </div>
        </div>
    </div>
</x-layout>