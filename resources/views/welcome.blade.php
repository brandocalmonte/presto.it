<x-layout>


    @if (session()->has('message'))
        <div class="alert alert-sucess">
            {{ session('message') }}
        </div>
    @endif
    @if (session()->has('access.denied'))
        <div class="alert alert-sucess">
            {{ session('access.denied') }}
        </div>
    @endif


    <!--HEADER-->
    <div class="container-fluid">
        <div class="row bg-head-image">

            <div class="col-12 col-lg-5 d-flex flex-column align-items-center justify-content-center margin-header">
                <h1 class=" text-bl fw-bolder text-center">{{__('ui.welcomeHeader')}}</h1>
                @guest
                <a type="submit" href="{{route('register')}}"class=" btn custom1-btn btn-c">{{__('ui.registerNow')}}</a>
                @else
                <a type="submit" href="{{route('announcements.create')}}" class="btn custom1-btn btn-c">{{__('ui.buttonHeader')}}</a>
                @endguest
            </div>
            <div class="col-7">


            </div>


        </div>
    </div>
    <!--FINE HEADER-->

    <!--CATEGORY-->
    <div class="container my-3">
        <div class="row justify-content-center text-center">
            <div class="col-12 my-3">
                <h1 class="text-bl fw-bolder text-center">{{__('ui.titleCategories')}}</h1>
            </div>
          @foreach ($categories as $category)


                 <div class="my-5 mx-2 col-4 col-md-2 icon-hover d-flex justify-content-center" data-aos="flip-right">
                     <a href="{{route('categoryShow', compact('category'))}}" class="no-hover1 fa-3x text-o fas nav-link {{($category->icon)}}" >
                        @if(session('locale')=='it')
                <p class="lead text-bl my-1" >{{($category->name_it)}}</p></a>
                @elseif(session('locale')=='en')
                <p class="lead text-bl my-1" >{{($category->name_en)}}</p></a>
                @else
                <p class="lead text-bl my-1" >{{($category->name_ua)}}</p></a>
                @endif

                </div>
          @endforeach

        </div>
    </div>

    {{-- SWIPER UTLIMI ARTICOLI --}}

    <section class="px-0 container-fluid my-5">

            <div class="row justify-content-around mx-3">
                <div class="col-12">
                    <h1 class="text-bl fw-bolder text-center">{{__('ui.lastAnnouncements')}}</h1>
                </div>
                @forelse ($announcements as $announcement)
                    <div class="col-12 col-md-3 article-box1 mb-5 py-2 card my-5">
                        <div class="text-center imgBox">
                            <img src="{{ !$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(400, 300) : '/media/placeholder.png' }}"
                            alt="{{ $announcement->title }}" class="img-fluid my-2 img-article">

                        </div>
                        <div class="contentBox">
                            <h4 class="card-title text-center text-bl">{{ $announcement->title }}</h5>
                                 @if(session('locale')=='it')
                            <p class="p-article lead text-cards fw-bold text-center text-bl">{{__('ui.categoryAnn')}} : {{$announcement->category->name_it}}</p>
                            {{-- @dd($category) --}}
                            @elseif(session('locale')=='en')
                            <p class="p-article lead text-cards fw-bold text-center text-bl">{{__('ui.categoryAnn')}} : {{$announcement->category->name_en}}</p>
                            @else
                            <p class="p-article lead text-cards fw-bold text-center text-bl">{{__('ui.categoryAnn')}} : {{$announcement->category->name_ua}}</p>
                            @endif
                            <p class="p-article lead text-bl text-cards fw-bold text-end">€ {{ $announcement->price }}</p>
                            <p class="p-article lead text-bl text-cards fst-italic pt-2 text-end">{{__('ui.author')}} : {{ $announcement->user->name }}</p>
                             <a href="{{ route('announcements.show', compact('announcement')) }}" class="buy">{{__('ui.details')}}</a>


                        </div>

             </div>
                        @empty
                        <div class="col-12">
                            <div class="alert alert-warning py-3 shadow">
                                <p class="lead">{{__('ui.noResults')}}</p>
                            </div>
                        </div>
                        @endforelse
            </div>


    </section>



</x-layout>
