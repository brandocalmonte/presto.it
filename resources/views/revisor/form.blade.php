<x-layout>
    <x-slot name="title">{{__('ui.becomeRevisor')}}</x-slot>

<div>

    <h1 class="text-o text-center">{{__('ui.becomeRevisor')}}</h1>

    <form class="container py-5" method="POST"
     action="{{route('become.revisor')}}">

        @csrf

        @if (session()->has('message'))
            <div class="alert alert-sucess">
                {{ session('message') }}
            </div>
        @endif




                        <label class="h5" for="description">{{__('ui.writeDescription')}}</label>
                            <textarea  name="description" class="form-control h-custom " placeholder="Scrivi una descrizione su di te" id="description"></textarea>

        </div>
        <div class="container">
            <div class="row justify-content-center">
            <button type="submit" class="btn custom1-btn btn-c text-o  shadow">{{__('ui.send')}}</button>

            </div>

        </div>


    </form>





</div>
</x-layout>