<x-layout>
    <x-slot name="title">{{__('ui.revisorZone')}}</x-slot>

    @if (session()->has('message'))
    <div class="alert alert-sucess">
        {{ session('message') }}
    </div>
    @endif

    <div class="container-fluid p-5 mb-4">
        <div class="row">
            <div class="col-12 text-o text-center">
                <h1 class="display-5 border-bottom">
                    {{$announcement_to_checks ? 'Annunci da revisionare:' : 'Non ci sono annunci da revisionare'}}
                </h1>
            </div>
        </div>
    </div>



@if($announcement_to_checks)
<div class="container">
        <div class="row">
{{$announcement_to_checks->title}}</h1>
            <div class="col-12">
                <div class="row">
                        @if(count($announcement_to_checks->images) > 0)
                        @foreach ($announcement_to_checks->images as $image)

                    <div class="col-6">
                        <div class="power-slide">
                            <img src="{{$image->getUrl(400,300)}}" class="img-fluid" alt="">
                        </div><div class="swiper-slide">
                        </div>
                    </div>

                    <div class="col-3">
                 <h5 class="tc-accent">{{__('ui.checkImg')}}</h5>

                    <p>{{__('ui.adults')}} : <span class="{{$image->adult}}"></span></p>
                    <p>{{__('ui.satire')}} : <span class="{{$image->spoof}}"></span></p>
                    <p>{{__('ui.medicine')}} : <span class="{{$image->medical}}"></span></p>
                    <p>{{__('ui.violence')}} : <span class="{{$image->violence}}"></span></p>
                    <p>{{__('ui.racy')}} : <span class="{{$image->racy}}"></span></p>
                </div>
                <div class="col-3">
                @if ($image->labels)
                            @foreach($image->labels as $label)
                                <p class="d-inline">{{$label}}</p>
                            @endforeach
                        @endif
                </div>
                @else
                <div class="col-12">
                <div class="bg-placeholder d-flex justify-content-center align-items-center">
                    <p class="text-o display-4 fw-bold">Presto.it</p>
                </div>
                </div>
                    @endforeach

            </div>
            </div>


            <div class="col-12 flex-column align-items-center d-flex justify-content-center">
                <h4 class="text-bl fw-bold">{{$announcement_to_checks->title}}</h4>
                    <p class="lead text-bl fst-italic">{{$announcement_to_checks->description}}</p>
                    <p class="lead text-bl">€ {{$announcement_to_checks->price}}</p>
                    <p class="lead text-bl">{{__('ui.author')}} : {{$announcement_to_checks->user->name}}</p>

            </div>
            <div class="col-6">
                        <form action="{{route('revisor.accept_announcement' , ['announcement'=>$announcement_to_checks])}}" method="POST">
                            @csrf
                            @method('PATCH')
                            <button type="submit" class="btn btn-success btn-rounded p-1 shadow"><i class="fa-solid fa-circle-check fa-3x"></i></button>
                        </form>
                    </div>
                    <div class="col-6">
                        <form action="{{route('revisor.reject_announcement' , ['announcement'=>$announcement_to_checks])}}" method="POST">
                            @csrf
                            @method('PATCH')
                            <button type="submit" class="btn btn-danger btn-rounded p-1 shadow"><i class="fa-solid fa-circle-xmark fa-3x"></i></button>
                        </form>
                    </div>
        </div>
    </div>


</x-layout>
