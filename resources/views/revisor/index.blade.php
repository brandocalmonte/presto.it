<x-layout>
       <x-slot name="title">{{__('ui.revisorZone')}}</x-slot>

    @if (session()->has('message'))
    <div class="alert alert-sucess">
        {{ session('message') }}
    </div>
    @endif

    <div class="container-fluid p-5 mb-4">
        <div class="row">
            <div class="col-12 text-o text-center">
                <h1 class="display-5 border-bottom">
                    {{$announcement_to_checks ? 'Annunci da revisionare:' : 'Non ci sono annunci da revisionare'}}
                </h1>
            </div>
        </div>
    </div>



<div class="container">
@if($announcement_to_checks)

        <div class="row revisor-box">
            <div class="col-12">
                <div class="row justify-content-center">
                        @if(count($announcement_to_checks->images) > 0)
                        @foreach ($announcement_to_checks->images as $image)

                    <div class="col-6 d-flex justify-content-center py-5">
                        <div class="">
                            <img src="{{$image->getUrl(400,300)}}" class="img-fluid power-slide" alt="">
                        </div>
                    </div>

                    <div class="col-3 py-5 text-center">
                 <h5 class="tc-accent">{{__('ui.checkImg')}}</h5>

                    <p>{{__('ui.adults')}} : <span class="{{$image->adult}}"></span></p>
                    <p>{{__('ui.satire')}} : <span class="{{$image->spoof}}"></span></p>
                    <p>{{__('ui.medicine')}} : <span class="{{$image->medical}}"></span></p>
                    <p>{{__('ui.violence')}} : <span class="{{$image->violence}}"></span></p>
                    <p>{{__('ui.racy')}} : <span class="{{$image->racy}}"></span></p>
                </div>
                <div class="col-3 py-5 text-center">
                 <h5 class="tc-accent">{{__('ui.tags')}}</h5>

                @if ($image->labels)
                            @foreach($image->labels as $label)
                                <p class="d-inline">{{$label}} -</p>
                            @endforeach
                        @endif
                </div>

                @endforeach
                @else
                <div class="col-12 d-flex justify-content-center">
                <div class="bg-placeholder d-flex justify-content-center align-items-center">
                    <p class="text-o display-4 fw-bold">Presto.it</p>
                </div>
                </div>
                @endif

            </div>
            </div>


            <div class="py-5 col-12 flex-column align-items-center d-flex justify-content-center">
                <h4 class="text-bl fw-bold">{{$announcement_to_checks->title}}</h4>
                    <p class="lead text-bl fst-italic">{{$announcement_to_checks->description}}</p>
                    <p class="lead text-bl">€ {{$announcement_to_checks->price}}</p>
                    <p class="lead text-bl">{{__('ui.author')}} : {{$announcement_to_checks->user->name}}</p>

            </div>
            <div class="col-4 d-flex justify-content-center mb-3">
                        <form action="{{route('revisor.accept_announcement' , ['announcement'=>$announcement_to_checks])}}" method="POST">
                            @csrf
                            @method('PATCH')
                            <button type="submit" class="btn btn-success btn-rounded p-1 shadow"><i class="fa-solid fa-circle-check fa-3x"></i></button>
                        </form>
                    </div>
                    <div class="col-4 d-flex justify-content-center">
                        <form action="{{route('revisor.reject_announcement' , ['announcement'=>$announcement_to_checks])}}" method="POST">
                            @csrf
                            @method('PATCH')
                            <button type="submit" class="btn btn-danger btn-rounded p-1 shadow"><i class="fa-solid fa-circle-xmark fa-3x"></i></button>
                        </form>
                    </div>
                    <div class="col-4 d-flex justify-content-center">
                        <form action="{{route('revisor.history_announcement' , ['announcement'=>$announcement_to_checks])}}" method="GET">
                            @csrf
                            @method('PATCH')
                            <button type="submit" class="btn btn-grey  p-1 "><i class="fas fa-list-ul fa-3x"></i></i></button>
                        </form>
                    </div>
        </div>
        @else
        <div class="row justify-content-around align-items-center">
            <div class="col-12 col-lg-6 text-center">
                <img src="/media/revisor.png" alt="" class="img-fluid">
            </div>
            <div class="col-12 col-lg-5">
                <p class="display-3 border-bottom text-center">{{__('ui.congrats')}}</p>
                <p class="fst-italic text-center fs-4 fw-light">{{__('ui.checkedAll')}}</p>
        </div>
    </div>
    @endif

    </div>


</x-layout>