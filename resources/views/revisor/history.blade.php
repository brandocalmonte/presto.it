<x-layout>
<x-slot name="title">{{__('ui.revisorZone')}}</x-slot>

    @if (session()->has('message'))
    <div class="alert alert-sucess">
        {{ session('message') }}
    </div>
    @endif

    <div class="container py-5">

        <div class="row justify-content-center">
            <div class="col-12">
                <h2 class="text-center text-o border-bottom mb-3 display-4">Annunci accettati</h2>
            </div>
            <div class="col-6 ">
                @foreach($announcements as $announcement)
                   @if($announcement -> is_accepted )
                <button class="accordion accordion-custom ">{{$announcement->title}}</button>
            <div class="panel  border-accordion-custom row justify-content-center m-0">
                <div class="col-4 d-flex justify-content-center align-items-center py-4 ">
                    <img src="{{ !$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(400, 300) : '/media/placeholder.png' }}"
                            alt="{{ $announcement->title }}" class="img-fluid img-article">
                </div>
                <div class="col-6 d-flex flex-column justify-content-center align-items-center py-4">
                    <p class=" text-bl fst-italic">{{$announcement->description}}</p>
                    <p class="fw-bold text-bl">€ {{$announcement->price}}</p>
                    <p class="text-bl">{{__('ui.author')}} : {{$announcement->user->name}}</p>
            </div>
            <div class="col-2 d-flex justify-content-center align-items-center ">
                <form action="{{route('revisor.forget_announcement' , ['announcement'=>$announcement])}}" method="POST">
                            @csrf
                            @method('PATCH')
                            <button type="submit" class="btn btn-primary btn-rounded p-1 shadow"><i class="fas fa-undo-alt fa-2x"></i></button>
                        </form>
            </div>
        </div>
            @endif
            @endforeach


        </div>
        <div class="col-12 mt-5">
                <h2 class="text-center text-o border-bottom mb-3 display-4">Annunci rifiutati</h2>
            </div>
            <div class="col-6">
                @foreach($announcements as $announcement)
                   @if(!$announcement -> is_accepted )
                <button class="accordion accordion-custom ">{{$announcement->title}}</button>
            <div class="panel  border-accordion-custom row justify-content-center m-0">
                <div class="col-4 d-flex justify-content-center align-items-center py-4 ">
                    <img src="{{ !$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(400, 300) : '/media/placeholder.png' }}"
                            alt="{{ $announcement->title }}" class="img-fluid img-article">
                </div>
                <div class="col-6 d-flex flex-column justify-content-center align-items-center py-4">
                    <p class=" text-bl fst-italic">{{$announcement->description}}</p>
                    <p class="fw-bold text-bl">€ {{$announcement->price}}</p>
                    <p class="text-bl">{{__('ui.author')}} : {{$announcement->user->name}}</p>
            </div>
            <div class="col-2 d-flex justify-content-center align-items-center ">
                <form action="{{route('revisor.forget_announcement' , ['announcement'=>$announcement])}}" method="POST">
                            @csrf
                            @method('PATCH')
                            <button type="submit" class="btn btn-primary btn-rounded p-1 shadow"><i class="fas fa-undo-alt fa-2x"></i></button>
                        </form>
            </div>
        </div>
            @endif
            @endforeach


        </div>
    </div>
</div>


</x-layout>