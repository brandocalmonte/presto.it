<x-layout>
    <x-slot name="title">{{__('ui.editYourInfo')}}</x-slot>

    <div class="container-fluid pt-4">
        <div class="row">
            <div class="col-12 text-o pb-5 text-center">
                <h1 class="display-5 border-bottom">
                    {{__('ui.editYourInfo')}}
                </h1>
            </div>
        </div>
    </div>

    @if(session('message'))
        <div class="col-12 alert alert-success">
            {{session('message')}}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    @if (session('status') === 'profile-information-updated')
        <div class="mb-4 alert alert-success">
            {{__('ui.profileEdited')}}
        </div>
    @endif

    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                <h2 class="text-o">{{__('ui.myInfo')}}</h2>

                @if(isset(Auth::user()->email_verified_at))
                    <p class="text-success"><i class="mx-2 fas fa-user-check text-success"></i>{{__('ui.userVerified')}}</p>
                @else

                    <p class="text-danger"><i class="mx-2 fas fa-user-times text-danger"></i>{{__('ui.userNotVerified')}}</p>

                @endif

                <form class="p-5 border shadow" method="POST" action="/user/profile-information">
                    @csrf
                    @method('put')

                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label ">{{__('ui.email')}}</label>
                      <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{Auth::user()->email}}">
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputUsername" class="form-label">{{__('ui.username')}}</label>
                        <input type="text" name="name" class="form-control" id="exampleInputUsername" aria-describedby="emailHelp" value="{{Auth::user()->name}}">
                    </div>

                    <button type="submit" class="btn custom1-btn btn-c">{{__('ui.updateInfo')}}</button>
                </form>

            </div>
        </div>
        <div class="row justify-content-center mt-5">
            <div class="col-12 col-md-8">
                @if (session('status') === 'password-updated')
                    <div class="mb-4 alert alert-success">
                        {{__('ui.passUpdated')}}
                    </div>
                @endif

                <h2 class="text-o">{{__('ui.myPassword')}}</h2>

                <form class="p-5 border shadow" method="POST" action="/user/password">
                    @csrf
                    @method('put')

                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">{{__('ui.actualPass')}}</label>
                        <input type="password" name="current_password" class="form-control" id="exampleInputPassword1">
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputNewPassword" class="form-label">{{__('ui.newPass')}}</label>
                        <input type="password" name="password" class="form-control" id="exampleInputNewPassword">
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputPasswordConfirmation" class="form-label">{{__('ui.confNewPass')}}</label>
                        <input type="password" name="password_confirmation" class="form-control" id="exampleInputPasswordConfirmation">
                    </div>

                    <button type="submit" class="btn custom1-btn btn-c">{{__('ui.updatePass')}}</button>
                </form>

            </div>
        </div>

    </div>



</x-layout>