

<div>
    <h1>Un utente ha richiesto di lavorare con noi</h1>
    <h2>Ecco i suoi dati: </h2>
    <p>Nome: {{$user->name}}</p>
    <p>Email: {{$user->email}}</p>
    <p>Descrizione:</p>
    <p>{{$user_contact['description']}}</p>

    <p>Se vuoi renderlo visibile clicca qui:</p>
    {{-- @dd($user) --}}

    <a href="{{route('make.revisor' , compact('user'))}}">Rendi revisore</a>
    {{-- @dd($user) --}}

</div>
