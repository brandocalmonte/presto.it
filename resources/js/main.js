var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}
let cardDesc = document.querySelectorAll('.js-desc-string');
if(cardDesc){
  function truncateString(str){if(str.length > 40){return str.split(' ')[0] + '...'}}
}
  truncateString()
