<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'La tua password è stata aggiornata!',
    'sent' => 'Ti abbiamo inviato una email con il link per aggiornare la password!',
    'throttled' => 'Per favore attenti prima di riprovare.',
    'token' => 'Il token per l\'aggiornamento della password non è valido.',
    'user' => "Non riusciamo a trovare utenti non questo indirizzo email.",

];
