<?php
return[

    //WELCOME
        //NAVBAR
        'navHome'=> 'Home',
        'navAddAnnounce'=>'Inserisci un annuncio',
        'navRevisor'=>'Zona revisore',
        'navAllAnnouncements'=>'Tutti gli annunci',
        'navLang' => 'Lang',
        'navLogReg'=>'Accedi/Registrati',
        'navWelcomeName'=>'Benvenuto',
        'navAdmin'=> 'Zona Admin',
        'profile' => 'Profilo',
        'logout'=> 'Logout',

        // HEADER
        'welcomeHeader'=> 'Inserisci presto il tuo primo annuncio!',
        'registerNow'=> 'Registrati ora!',
        'buttonHeader'=> 'Clicca qui!',

        // CATEGORY TITLE
        'titleCategories'=>'Le nostre categorie',
        'lastAnnouncements'=>'I nostri ultimi annunci',

        // FOOTER
        'contactsFooter'=> 'Contatti',
        'linkFooter'=> 'Links',
        'newsletterFooter'=> 'Iscriviti alla Newsletter',
        'linkHome'=> 'Home',
        'linkAddAnouc'=> 'Inserisci un annuncio',
        'linkShowAnounc'=> 'Vedi annunci',
        'linkAboutUs'=>'Chi siamo',
        'linkWorkUs'=> 'Lavora con noi',
        'btnFooter'=> 'Iscriviti',

          // {{__('ui.navAddAnnounce')}}

    //AUTH
        //REGISTER
        'register' => 'Registrati',
        'name' => 'Nome',
        'surname' => 'Cognome',
        'gender' => 'Seleziona il sesso',
        'email' => 'Email',
        'password' => 'Password',
        'passwordConf' => 'Conferma password',

        //LOGIN
        'login' => 'Login',
        'signIn' => 'Accedi',
        'forgotPassword' => 'Hai dimenticato la password?',
        'notRegistered' => 'Non sei registrato?',

        //FORGOT PASSWORD
        'forgotPasswordPage' => 'Password dimenticata',
        'resetPassword' => 'Reset password',

        //VERIFY EMAIL
        'thanks4Subscribe' => 'Grazie per esserti iscritto!',
        'verifyEmail' => 'Verifica Email',
        'guideVerifyEmail' => 'Grazie per esserti iscritto! Prima di cominciare, verifica il tuo indirizzo email clickando sul link che ti abbiamo appena inviato!',
        'linkSent' => 'Ti è stato inviato un nuovo link!',
        'sendMailAgain' => 'Reinvia email',

    //ADD ANNOUNCEMENT
        //FORM
        'createAnn' => 'Crea il tuo annuncio!',
        'annName' => 'Titolo del tuo annuncio',
        'commentsAnn' => 'Commenti',
        'categoryAnn' => 'Categoria',
        'selectCategoryAnn' => 'Scegli la categoria',
        'photoPreview' => 'Anteprima foto inserite:',
        'deletePhoto' => 'Cancella',
        'priceAnn' => 'Prezzo',
        'btnCreateAnn' => 'Crea',

    //ALL ANNOUNCEMENTS
        //HEADER
        'allAnn' => 'Tutti gli annunci',
        'searchByName' => 'Cerca per nome',

        //CATEGORY CARD
        'author' => 'Creato da',
        'details' => 'Dettagli',
        'noResults' => 'Non ci sono annunci per questa ricerca. Prova a cambiare parola',

        //EDIT ANNOUNCEMENT
        'editAnn' => 'Modifica il tuo annuncio',

        //ANNOUNCEMENT DETAILS
        'titleAnn' => 'Annuncio',
        'btnEdit' => 'Modifica',
        'userAnn' => 'I tuoi annunci',

    //SHOW CATEGORIES
        'exploreCategory' => 'Esplora la categoria',
        'noAnnXCat' => 'Non sono presenti annunci per questa categoria!',
        'postAnn' => 'Pubblicane uno',
        'newAnnouncement' => 'Nuovo Annuncio',

    //REVISOR ZONE
        'becomeRevisor' => 'Richiedi di diventare revisore!',
        'writeDescription' => 'Scrivi una descrizione su di te',
        'send' => 'Invia',

        'revisorZone' => 'Zona revisore',
        'tags' => 'Tags',
        'checkImg' => 'Revisione Immagini',
        'adults' => 'Adulti',
        'satire' => 'Satira',
        'medicine' => 'Medicina',
        'violence' => 'Violenza',
        'racy' => 'Contenuto Ammiccante',

        'congrats' => 'Complimenti',
        'checkedAll' => 'Hai revisionato tutti gli annunci, prenditi una pausa',

    //ADMIN ZONE
        'titleAdmin' => 'Pannello Admin',
        'textAdmin' => 'Sfrutta la possibilità di comunicare a tutti gli iscritti le ultime novità!',
        'sendNewsletter' => 'Invia NewsLetter',

    //NEWSLETTER
        'sendNewsletter' => 'Invia Newsletter',
        'newsletterTitle' => 'Titolo della Newsletter',
        'newsletterDescr' => 'Descrizione',
        'newsletterCreate' => 'Crea',
        'contactUs' => 'Contattaci',
        'ourProducts' => 'I nostri prodotti',


    //FAQ
        'howToRegister' => 'Come registrarsi?',
        'howToPost' => 'Come inserire un annuncio?',
        'howToEditPass' => 'Come modificare la password?',
        'wantToWorkWithUs' => 'Vuoi lavorare con noi?',

    //EDIT PROFILE
        'editInfo' => 'Modifica dati',
        'whatToEdit' => 'Hai bisogno di cambiare email o password?',
        'clickHere' => 'Clicca qui',
        'lookYourAnn' => 'Guarda i tuoi annunci',
        'checkYourAnn' => 'Tieni d\' occhio tutti i tuoi annunci!',
        'editYourInfo' => 'Modifica i tuoi dati',
        'profileEdited' => 'Profilo aggiornato',
        'myInfo' => 'I miei dati',
        'userVerified' => 'Utente verificato',
        'userNotVerified' => 'Utente non verificato',
        'username' => 'Username',
        'updateInfo' => 'Aggiorna dati',
        'passUpdated' => 'Password aggiornata',
        'myPassword' => 'La mia password',
        'actualPass' => 'Password attuale',
        'newPass' => 'Nuova password',
        'confNewPass' => 'Conferma nuova password',
        'updatePass' => 'Aggiorna',


];