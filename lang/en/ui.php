<?php
return[

    //WELCOME
        //NAVBAR
        'navHome'=> 'Home',
        'navAddAnnounce'=>'Add Announcement',
        'navRevisor'=>'Revisor Area',
        'navAllAnnouncements'=>'All Announcements',
        'navLang' => 'Lang',
        'navLogReg'=>'Login/Sign In',
        'navWelcomeName'=>'Welcome',
        'navAdmin'=> 'Admin Area',
        'profile' => 'Profile',
        'logout'=> 'Logout',

        // HEADER
        'welcomeHeader'=> 'Add now your announcements!',
        'registerNow'=> 'Sign in now!',
        'buttonHeader'=> 'Click here!',

        // CATEGORY TITLE
        'titleCategories'=>'Our categories',
        'lastAnnouncements'=>'Our last announcements',

        // FOOTER
        'contactsFooter'=> 'Contacts',
        'linkFooter'=> 'Links',
        'newsletterFooter'=> 'Sign in to the Newsletter',
        'linkHome'=> 'Home',
        'linkAddAnouc'=> 'Add Announcement',
        'linkShowAnounc'=> 'Show Announcements',
        'linkAboutUs'=>'About Us',
        'linkWorkUs'=> 'Work with Us',
        'btnFooter'=> 'Sign In',

    //AUTH
        //REGISTER
        'register' => 'Sign In',
        'name' => 'Name',
        'surname' => 'Surname',
        'gender' => 'Select your gender',
        'email' => 'Email',
        'password' => 'Password',
        'passwordConf' => 'Confirm password',

        //LOGIN
        'login' => 'Login',
        'signIn' => 'Sign in',
        'forgotPassword' => 'Forgot your password?',
        'notRegistered' => 'Aren\'t you signed in?',

        //FORGOT PASSWORD
        'forgotPasswordPage' => 'Forgotten password',
        'resetPassword' => 'Reset password',

        //VERIFY EMAIL
        'thanks4Subscribe' => 'Thank you for the subscription!',
        'verifyEmail' => 'Verify Email',
        'guideVerifyEmail' => 'Thank you for the subscription! Before you start, check your email address by clicking on the link we\'ve just sent to you!',
        'linkSent' => 'A new link has just been sent!',
        'sendMailAgain' => 'Send a new email',    

    //ADD ANNOUNCEMENT
        //FORM
        'createAnn' => 'Create your announcement!',
        'annName' => 'Your announcement\'s title',
        'commentsAnn' => 'Comments',
        'categoryAnn' => 'Category',
        'selectCategoryAnn' => 'Select a category',
        'photoPreview' => 'Prhoto Preview:',
        'deletePhoto' => 'Delete',
        'priceAnn' => 'Price',
        'btnCreateAnn' => 'Create',

    //ALL ANNOUNCEMENTS
        //HEADER
        'allAnn' => 'All Announcements',
        'searchByName' => 'Search by name',

        //CARD CATEGORIE
        'author' => 'Author',
        'details' => 'Details',
        'noResults' => 'There are no result for your search. Try to change the word',

        //EDIT ANNOUNCEMENT
        'editAnn' => 'Edit your announcement',

        //ANNOUNCEMENT DETAILS
        'titleAnn' => 'Announce',
        'btnEdit' => 'Edit',  
        'userAnn' => 'Your announcements',

    //SHOW CATEGORIES
        'exploreCategory' => 'Explore the category',
        'noAnnXCat' => 'There are no announcements for this category yet!',
        'postAnn' => 'Be the first to post',
        'newAnnouncement' => 'New Announcement',

    //REVISOR ZONE
        'becomeRevisor' => 'Ask to become a revisor!',   
        'writeDescription' => 'Write a little description about yourself', 
        'send' => 'Send',

        'revisorZone' => 'Revisor Area',
        'tags' => 'Tags',
        'checkImg' => 'Images Revision',
        'adults' => 'Adults',
        'satire' => 'Satire',
        'medicine' => 'Medicine',
        'violence' => 'Violence',
        'racy' => 'Racy',

        'congrats' => 'Congratulations',
        'checkedAll' => 'You have checked all the announcements, take a little break',

    //ADMIN ZONE
        'titleAdmin' => 'Admin Area',
        'textAdmin' => 'Take the advantage of communicating to all subscribers the last news!',
        'sendNewsletter' => 'Send NewsLetter',


    //NEWSLETTER
        'sendNewsletter' => 'Send Newsletter',
        'newsletterTitle' => 'Newsletter\'s title',
        'newsletterDescr' => 'Description',
        'newsletterCreate' => 'Create',
        'contactUs' => 'Contact Us',
        'ourProducts' => 'Our products',

     //FAQ
        'howToRegister' => 'How to sign in?',
        'howToPost' => 'How to post an announcement?',  
        'howToEditPass' => 'How to edit your password?', 
        'wantToWorkWithUs' => 'Want to work with us?',

    //EDIT PROFILE
        'editInfo' => 'Edit info',
        'whatToEdit' => 'Do you need to change your email or password?',
        'clickHere' => 'Click here',
        'lookYourAnn' => 'See your announcements',
        'checkYourAnn' => 'Check your announcements!',
        'editYourInfo' => 'Edit your infos',
        'profileEdited' => 'The profile is now updated',
        'myInfo' => 'My infos',
        'userVerified' => 'User verified',
        'userNotVerified' => 'User not verified',
        'username' => 'Username',
        'updateInfo' => 'Update infos',
        'passUpdated' => 'Password updated',
        'myPassword' => 'My password',
        'actualPass' => 'Actual password',
        'newPass' => 'New password',
        'confNewPass' => 'Confirm new password',
        'updatePass' => 'Update password',    
];