<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Недійсні облікові дані.',
    'password' => 'Невірний пароль.',
    'throttle' => 'Забагато спроб входу. Будь ласка, спробуйте ще раз через :seconds секундЗабагато спроб входу. Будь ласка, спробуйте ще раз через :секунд секунд.',

];
