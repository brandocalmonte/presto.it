<?php
return[

    //WELCOME
        //NAVBAR
        'navHome'=> 'Головна',
        'navAddAnnounce'=>'Добавити оголошення',
        'navRevisor'=>'Зона Аудитора',
        'navAllAnnouncements'=>'Всі оголошення',
        'navLang' => 'Мова',
        'navLogReg'=>'Увійти/Зареєструватися',
        'navWelcomeName'=>'Ласкаво Просимо',
        'navAdmin'=> 'Зона Адміна',
        'profile' => 'Профіль',
        'logout'=> 'Вийти',

        // HEADER
        'welcomeHeader'=> 'Добавляй скоріше свої оголошення!',
        'registerNow'=> 'Зареєструйся!',
        'buttonHeader'=> 'Натисни тут!',

        // CATEGORY TITLE
        'titleCategories'=>'Наші категоріі',
        'lastAnnouncements'=>'Наші останні оголошення',
        
        // FOOTER
        'contactsFooter'=> 'Контакти',
        'linkFooter'=> 'Посилання',
        'newsletterFooter'=> 'Підпишись зараз на Newsletter',
        'linkHome'=> 'Головна',
        'linkAddAnouc'=> 'Добавити оголошення',
        'linkShowAnounc'=> 'Всі оголошення',
        'linkAboutUs'=>'Хто ми',
        'linkWorkUs'=> 'Працюй з нами',
        'btnFooter'=> 'Підпишись',
    
    //AUTH
        //REGISTER
        'register' => 'Зареєструватися',
        'name' => 'Імʼя',
        'surname' => 'Прізвище',
        'gender' => 'Стать',
        'email' => 'Ємейл',
        'password' => 'Пароль',
        'passwordConf' => 'Підтвердити пароль',

        //LOGIN
        'login' => 'Логін',
        'signIn' => 'Увійти',
        'forgotPassword' => 'Забув пароль?',
        'notRegistered' => 'Ще не зареєструвався?',

        //FORGOT PASSWORD
        'forgotPasswordPage' => 'Забув пароль',
        'resetPassword' => 'Відновити пароль',

        //VERIFY EMAIL
        'thanks4Subscribe' => 'Дякую за підписку!',
        'verifyEmail' => 'Провірити Ємейл',
        'guideVerifyEmail' => 'Дякую за підписку! Перш ніж почати, підтвердьте свою адресу електронної пошти, натиснувши посилання, яке ми вам щойно надіслали!',
        'linkSent' => 'Вам щойно надіслали нове посилання!',
        'sendMailAgain' => 'Відправити знову ємейл',    

    //ADD ANNOUNCEMENT
        //FORM
        'createAnn' => 'Cтвори нове оголошення!',
        'annName' => 'Назва оголошення',  
        'commentsAnn' => 'Комментарі',
        'categoryAnn' => 'Категорія',
        'selectCategoryAnn' => 'Виберіть категорію',
        'photoPreview' => 'Попередній перегляд :',
        'deletePhoto' => 'Видалити',
        'priceAnn' => 'Ціна',
        'btnCreateAnn' => 'Cтворити',

    //TUTTI GLI ANNUNCI
        //HEADER
        'allAnn' => 'Всі оголошення',
        'searchByName' => 'Пошук за назвою',

        //CATEGORY CARD
        'author' => 'Автор',
        'details' => 'Деталі',
        'noResults' => 'Для цього пошуку немає результатів. Спробуй змінити слово',

        //EDIT ANNOUNCEMENT
        'editAnn' => 'Змінити оголощення',

        //ANNOUNCEMENT DETAILS
        'titleAnn' => 'Оголошення',
        'btnEdit' => 'Змінити',  
        'userAnn' => 'Ваші оголошення',

    //SHOW CATEGORIES
        'exploreCategory' => 'Всі результати на категорію',
        'noAnnXCat' => 'Нажаль ще немає оголошень в цій категоріі!',
        'postAnn' => 'Опублікуй його першим',
        'newAnnouncement' => 'Нове оголошення',    

    //REVISOR ZONE
        'becomeRevisor' => 'Зроби запрос щоб стати Аудитором!',   
        'writeDescription' => 'Напиши невеликий опис про тебе', 
        'send' => 'Відправити',

        'revisorZone' => 'Зона Аудитора',
        'tags' => 'Таг',
        'checkImg' => 'Контроль картинок',
        'adults' => 'Дорослі',
        'satire' => 'Сатіра',
        'medicine' => 'Медицина',
        'violence' => 'Насильство',
        'racy' => 'Расизм',

        'congrats' => 'Вітаємо',
        'checkedAll' => 'Ви переглянули всі оголошення, можете зробити перерву',


    //ADMIN 
        'titleAdmin' => 'Зона Адміна',
        'textAdmin' => 'Спіймай можливість що б повідомляти останні новини всім абонентам!',
        'sendNewsletter' => 'Відправити',   
        
     //NEWSLETTER
        'sendNewsletter' => 'Відправити',    
        'newsletterTitle' => 'Назва Newsletter',
        'newsletterDescr' => 'Опис',
        'newsletterCreate' => 'Створити',  
        'contactUs' => 'Зв\'яжіться з нами',
        'ourProducts' => 'Наші оголошення',  

    //FAQ
        'howToRegister' => 'Як зареєструватися?',
        'howToPost' => 'Як добавити оголошення?',  
        'howToEditPass' => 'Як змінити пароль?', 
        'wantToWorkWithUs' => 'Хочешь працювати з нами?',

    //EDIT PROFILE
        'editInfo' => 'Змінити дані',
        'whatToEdit' => 'Хочешь змінити ємейл чи пароль?',
        'clickHere' => 'Натисни тут',
        'lookYourAnn' => 'Подивітся свої оголошення',
        'checkYourAnn' => 'Стежіть за всімa cвоїми оголошеннями!',
        'editYourInfo' => 'Змінитини дані',
        'profileEdited' => 'Профіль відновлен',
        'myInfo' => 'Мої дані',
        'userVerified' => 'Користувач перевірений',
        'userNotVerified' => 'Користувач не перевірений',
        'username' => 'Iм\'я користувача',
        'updateInfo' => 'Відновити дані',
        'passUpdated' => 'Пароль змінено',
        'myPassword' => 'Мій пароль',
        'actualPass' => 'Теперешній пароль',
        'newPass' => 'Новий пароль',
        'confNewPass' => 'Підтвердити новий пароль',
        'updatePass' => 'Відновити',    
            
];