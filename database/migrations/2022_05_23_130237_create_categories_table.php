<?php

use App\Models\Category;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();

            $table->string('name_it');
            $table->string('name_en');
            $table->string('name_ua');
            $table->string('icon');
            $table->timestamps();
        });
        // $categories = ['Lavoro', 'Viaggi', 'Videogames', 'Elettrodomestici', 'Abbigliamento', 'Motori', 'Arredamento', 'Immobili', 'Tecnologia', 'Giardinaggio'];
        // $classes = ['fa-briefcase', 'fa-plane', 'fa-gamepad ', 'fa-blender ', 'fa-tshirt ', ' fa-car', 'fa-couch ', ' fa-home', 'fa-desktop ',  'fa-tree '];
        $categories = [
            ['Lavoro', 'Job', 'Робота', 'fa-briefcase'],
            ['Viaggi', 'Travel', 'Поїздки', 'fa-plane'],
            ['Videogiochi', 'Videogames', 'Відео ігри', 'fa-gamepad'],
            ['Elettrodomestici', 'Domestic appliances', 'Побутова техніка', 'fa-blender'],
            ['Abbigliamento', 'Clothes', 'Одяг', 'fa-tshirt'],
            ['Motori', 'Motors', 'Двигуни', 'fa-car' ],
            ['Arredamento', 'Furniture', 'Меблі', 'fa-couch'],
            ['Immobili', 'Real estate', 'Властивості', 'fa-home'],
            ['Tecnologia', 'Tech', 'Технологія', 'fa-desktop'],
            ['Giardinaggio', 'Gardening', 'Cадівництво', 'fa-tree '],
        ];
        foreach($categories as $category){
            Category::create([
                'name_it' => $category[0],
                'name_en' => $category[1],
                'name_ua' => $category[2],
                'icon' => $category[3],
            ]);
        }
        // foreach($categories as $category){
        //     Category::create(['name' => $category]);
        // }
        // for ($i=0; $i < 10 ; $i++) {
        //    Category::create(['name' => $categories[$i],
        // 'icon' => $classes[$i]]);
        // }
        }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
};
