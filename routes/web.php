<?php

use App\Models\Announcement;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\AnnouncementEdit;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\RevisorController;
use App\Http\Controllers\AnnouncementController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontController::class, 'welcome'])->name('welcome');
Route::get('/profile', [FrontController::class, 'profile'])->middleware('auth')->name('profile');
Route::get('/profile/edit', [FrontController::class, 'profileEdit'])->middleware('auth')->name('profileEdit');
Route::get('/categoria/{category}', [FrontController::class, 'categoryShow'])->name('categoryShow');
Route::get('/faq', [FrontController::class, 'faq'])->name('faq');
Route::get('/terms', [FrontController::class, 'terms'])->name('terms');
Route::get('/privacy', [FrontController::class, 'privacy'])->name('privacy');




// Rotta Annuncio
Route::get('/announcement/create', [AnnouncementController::class, 'announcementCreate'])->middleware('verified')->name('announcements.create');
Route::get('/announcement/index', [AnnouncementController::class, 'announcementIndex'])->name('announcements.index');
Route::get('/announcement/edit/{announcement}', [AnnouncementController::class, 'announcementEdit'])->middleware('verified')->name('announcements.edit');
Route::get('/announcement/show/{announcement}', [AnnouncementController::class, 'announcementShow'])->name('announcements.show');
Route::get('/announcement/user', [AnnouncementController::class, 'announcementUser'])->name('announcements.user');



// Home Revisor
Route::get('/revisor/home' , [RevisorController::class, 'index'])->middleware('isRevisor')->name('revisor.index');
// Accetta annuncio
Route::patch('/accetta/annuncio/{announcement}' , [RevisorController::class, 'acceptAnnouncement'])->middleware('isRevisor')->name('revisor.accept_announcement');
// Rifiuta annuncio
Route::patch('/rifiuta/annuncio/{announcement}' , [RevisorController::class, 'rejectAnnouncement'])->middleware('isRevisor')->name('revisor.reject_announcement');
//history
Route::get('/history/annuncio/{announcement}', [RevisorController::class, 'allAnnouncement'])->middleware('isRevisor')->name('revisor.history_announcement');
// Annulla accetta/rifiuta
Route::patch('/annulla/annuncio/{announcement}', [RevisorController::class, 'forgetAnnouncement'])->middleware('isRevisor')->name('revisor.forget_announcement');
//Form di richiesta revisore
Route::get('/compila-richiesta/revisore', [RevisorController::class, 'formRevisor'])->middleware('verified')->name('revisor.form');
//Richiedi di diventare revisore
Route::post('/richiesta/revisore', [RevisorController::class, 'becomeRevisor'])->middleware('auth')->name('become.revisor');
//Rendi utente revisore
Route::get('/rendi/revisore/{user}', [RevisorController::class, 'makeRevisor'])->name('make.revisor');

//unsubscribe newsletter
Route::get('/unsubscribe', [AdminController::class, 'UnsubscribeNewsletter'])->name('unsubscribe.newsletter');



//Ricerca annuncio
Route::get('/ricerca/annuncio', [FrontController::class, 'search_announcements'])->name('announcements.search');

//ROTTE ADMIN
Route::get('/admin/home' , [AdminController::class, 'index'])->middleware('isAdmin')->name('admin.index');
Route::get('/admin/newsletter' , [AdminController::class, 'newsletter'])->middleware('isAdmin')->name('admin.newsletter');
Route::post('/send/newsletter' , [AdminController::class, 'send_newsletter'])->middleware('isAdmin')->name('admin.send-newsletter');

// CAMBIO LINGUA
Route::post('/lingua/{lang}' , [FrontController::class, 'setLanguage'])->name ('set_language_locale');